'''
towniesim.family
================
'''
from enum import Enum
from random import choice
from .race import Age
from .npc import NPC
from .development import Development
from .building import Building
from .util import even_switch, nds, gauss_mm


class FamilyType(Enum):
    normal = 'husband, wife and children'
    single_father = 'father and children'
    single_mother = 'mother and children'
    siblings = 'siblings'
    single_male = 'single male'
    single_female = 'single female'


class Family:

    TYPES = (
        (50, FamilyType.normal),
        (15, FamilyType.single_male),
        (15, FamilyType.single_female),
        (10, FamilyType.siblings),
        (5, FamilyType.single_father),
        (5, FamilyType.single_mother),
    )

    def __init__(self, race=None):
        self.name = None
        self.mother = None
        self.father = None
        self.children = set()
        self.family_type = even_switch(self.TYPES, nds(1, 100))
        self.race = race
        self.wealth = 0
        self.buildings = set()
        self.workplaces = set()
        if self.family_type == FamilyType.normal:
            self.father = self.create_male(age=Age.random_parent())
            self.mother = self.create_female(age=Age.random_parent())
            self.mother.take_last_name(self.father)
            self.children = self.create_children()
            for child in self.children:
                child.take_last_name(self.father)
            self.name = self.father.get_last_name()
        elif self.family_type == FamilyType.single_father:
            self.father = self.create_male(age=Age.random_parent())
            self.children = self.create_children()
            for child in self.children:
                child.take_last_name(self.father)
            self.name = self.father.get_last_name()
        elif self.family_type == FamilyType.single_mother:
            self.mother = self.create_female(age=Age.random_parent())
            self.children = self.create_children()
            for child in self.children:
                child.take_last_name(self.mother)
            self.name = self.mother.get_last_name()
        elif self.family_type == FamilyType.single_male:
            self.father = self.create_male(age=Age.random_notchild())
            self.name = self.father.get_last_name()
        elif self.family_type == FamilyType.single_female:
            self.mother = self.create_female(age=Age.random_notchild())
            self.name = self.mother.get_last_name()
        elif self.family_type == FamilyType.siblings:
            self.children = self.create_children(min_num=2,
                                                 age=Age.random_parent())
            children = list(self.children)
            for child in children[1:]:
                child.take_last_name(children[0])
            self.name = children[0].get_last_name()
        else:
            raise ValueError('No family type: {!r}'.format(self.family_type))
        self.link_family()

    def create_male(self, age=None):
        age = age or Age.random()
        new = NPC(race=self.race, gender='male', age=age)
        return new

    def create_female(self, age=None):
        age = age or Age.random()
        new = NPC(race=self.race, gender='female', age=age)
        return new

    def create_children(self, min_num=1, age=None):
        children = set()
        for i in range(int(gauss_mm(2, 1, min_num, 9))):
            func = choice([self.create_male, self.create_female])
            new = func(age=age)
            children.add(new)
        return children

    def link_family(self):
        if self.father:
            self.father.lover = self.mother
            self.father.family = self
        if self.mother:
            self.mother.lover = self.father
            self.mother.family = self
        for child in self.children:
            child.parents_family = self

    def members(self):
        if self.father:
            yield self.father
        if self.mother:
            yield self.mother
        if self.children:
            for child in self.children:
                yield child

    def assign_workplace(self, workplace):
        members = set()
        if isinstance(workplace, Development):
            jobtype = workplace.WORKER
        elif isinstance(workplace, Building):
            jobtype = choice(workplace.WORKERS)
        else:
            raise ValueError('Cant assign workplace: {!r}'.format(workplace))
        for mem in self.members():
            if mem.get_age() in (Age.child, Age.venerable):
                continue
            members.add(mem)
            job = jobtype(location=workplace)
            mem.set_job(job)
