'''
towniesim.town
==============
'''
from datetime import datetime
from random import shuffle, choice
from .util import nds, le_switch
from .chrono import tick
from .development import Development, Farm
from .resource import Ores, Dirt, Lumber
from .residence import Residence
from .family import Family
from .area import Area, Position
from .config import TownConfig


class Town:

    def __init__(self, config_path=None, config=None):
        self.config = TownConfig(config_path=config_path, config=config)
        self.developments = {}
        self.resources = {}
        self.init_resources()
        self.npcs = set()
        self.residences = set()
        self.families = set()
        self.area = Area(self.config)
        self.now = datetime(1, 1, 1)
        self.settle()

    def __repr__(self):
        info = (
            'Town(\n\tresources={res!r},\n'
            '\tdevelopments={devs!r},\n'
            '\tresidences={rezs!r},\n'
            ')'
        ).format(res=sorted(self.resources.values(), reverse=True),
                 devs={k: v for k, v in self.developments.items() if v},
                 rezs=list(self.residences))
        info += '\n\n{!r}'.format(self.area)
        return info

    def init_resources(self):
        for res in (Dirt, Lumber):
            new = res.random()
            self.resources[res] = new
            self.developments[new] = []
        self.random_init_ores()

    def random_init_ores(self):
        ct = le_switch({4: 0, 10: 1, 16: 2, 19: 3, 20: 4}, nds(1, 20))
        all_ores = list(Ores)
        shuffle(all_ores)
        for ore in all_ores[:ct]:
            new = ore.random()
            self.resources[ore] = new
            self.developments[new] = []

    def create_development(self):
        most_val_res = sorted(self.resources.values())[-1]
        dev_cls = Development.get_from_resource(most_val_res)
        new_dev = dev_cls(town=self)
        self.developments[most_val_res] += [new_dev]
        new_dev.set_block(self.area.random_land_block())

    def list_developments(self):
        lst = []
        for _, devs in self.developments.items():
            lst += devs
        return lst

    def pick_random_development(self):
        return choice(self.list_developments())

    def settle(self):
        self.create_development()
        for i in range(self.config.num_farms()):
            farm = Farm()
            farm.set_block(self.area.random_land_block())
            self.developments[self.resources[Dirt]] += [farm]
        for i in range(self.config.num_families()):
            self.create_settler_family(workplace=self.pick_random_development())
        for npc in self.npcs:
            npc.make_plans(self.now)

    def create_settler_family(self, workplace=None):
        fam = Family(race=self.config.random_race())
        self.families.add(fam)
        rez = self.build_residence(fam)
        for mem in fam.members():
            self.npcs.add(mem)
            mem.pos = Position(rez)
        if workplace is not None:
            fam.assign_workplace(workplace)

    def build_residence(self, fam):
        rezs = Residence.get_residences(max_level=0)
        block = self.find_empty_town_block()
        rez = choice(rezs)(owner=fam, pos=block)
        self.residences.add(rez)
        return rez

    def find_empty_town_block(self):
        while True:
            block = self.area.random_town_block()
            if not block.full():
                break
        return block

    def tick(self):
        for npc in self.npcs:
            plan = npc.current_plan(self.now)
        # add minute to time
        old = self.now
        self.now = tick(self.now)
        if self.now.date() != old.date():
            for npc in self.npcs:
                npc.make_plans(self.now)

    def run(self, tdelta):
        end = self.now + tdelta
        while self.now < end:
            self.tick()
