'''
towniesim.behave
================
'''
from enum import Enum
from .chrono import within


class ActionPlan(Enum):
    null = 'null'
    work = 'work'
    home = 'home'
    rest = 'rest'
    shop = 'shop'
    drink = 'drink'
    walk = 'walk'
    sleep = 'sleep'


class Action(Enum):
    sit = 'sit'
    stand = 'stand'
    eat = 'eat'
    drink = 'drink'
    talk = 'talk'
    piss = 'piss'


class Plan:
    __slots__ = ('action', 'start', 'end')

    def __init__(self, action=None, start=None, end=None):
        self.action = action
        self.start = start
        self.end = end

    @classmethod
    def rand_sleep(cls, day, shift, hours=7):
        action = ActionPlan.sleep
        start, end = shift.rand_sleep(day, hours=hours)
        return cls(action=action, start=start, end=end)

    def __contains__(self, dt):
        return within(dt, (self.start, self.end))
