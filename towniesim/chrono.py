'''
towniesim.chrono
================
'''
from enum import Enum
from random import randint
from datetime import datetime, date, timedelta


def daystart(day):
    return day.replace(hour=0, minute=0, second=0)


def dayhour(day, hour):
    return day.replace(hour=hour, minute=0, second=0)


def tick(now):
    return now + timedelta(minutes=1)


class Shift(Enum):
    grave = (0, 8)
    day = (8, 16)
    mid = (12, 20)
    eve = (16, 24)

    def __contains__(self, arg):
        if isinstance(arg, (int, float)):
            return self.value[0] <= arg < self.value[1]
        if isinstance(arg, datetime):
            hour = arg.hour + (arg.minute / 60)
            return hour in self
        raise ValueError('argument must be int, float, or datetime')

    def dtrange(self, day):
        day = daystart(day)
        start = datetime(day.year, day.month, day.day, self.value[0])
        end = datetime(day.year, day.month, day.day, self.value[1])
        return start, end

    def rand_sleep(self, day, hours=7):
        if self is Shift.day:
            end = randint(5, 7)
        elif self is Shift.mid:
            end = randint(7, 11)
        elif self is Shift.eve:
            end = randint(8, 12)
        elif self is Shift.grave:
            end = randint(19, 23)
        else:
            raise NotImplementedError('rand_sleep not impl for {s.name}'.format(
                s=self))
        if end - hours < 0:
            hours = end
        return dayhour(day, end - hours), dayhour(day, end)


def within(dt, dtrange):
    return dtrange[0] <= dt < dtrange[1]
