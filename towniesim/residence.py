'''
towniesim.residence
===================
'''
from .building import MetaBuilding, Building


class MetaResidence(MetaBuilding):

    RESIDENCES = {}

    def __new__(cls, name, bases, dct):
        new_cls = super(MetaResidence, cls).__new__(cls, name, bases, dct)
        if name == 'Residence':
            return new_cls
        MetaResidence.RESIDENCES[dct['NAME']] = new_cls
        return new_cls


class Residence(Building, metaclass=MetaResidence):

    def __init__(self, **kwargs):
        super(Residence, self).__init__(**kwargs)

    @classmethod
    def get_residences(cls, max_level=None):
        rezs = MetaResidence.RESIDENCES.values()
        if max_level is not None:
            rezs = [r for r in rezs if r.LEVEL <= max_level]
        return rezs

    @classmethod
    def get_residence(cls, name):
        return MetaResidence.RESIDENCES[name]


class Cottage(Residence):
    NAME = 'cottage'
    LEVEL = 0


class Cabin(Residence):
    NAME = 'cabin'
    LEVEL = 0


class Home(Residence):
    NAME = 'home'
    LEVEL = 1


class TownHouse(Residence):
    NAME = 'townhouse'
    LEVEL = 2


class Mansion(Residence):
    NAME = 'mansion'
    LEVEL = 3
