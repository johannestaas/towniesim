'''
towniesim.shell
===============
'''
import os
import re
from datetime import timedelta
from cmd import Cmd
from .town import Town

INTRO = '''
  _______                  _       _____ _ __  ___
 |__   __|                (_)     / ____(_)  \/  |
    | | _____      ___ __  _  ___| (___  _| \  / |
    | |/ _ \ \ /\ / / '_ \| |/ _ \\\\___ \| | |\/| |
    | | (_) \ V  V /| | | | |  __/____) | | |  | |
    |_|\___/ \_/\_/ |_| |_|_|\___|_____/|_|_|  |_|

Welcome home!
'''

RE_TIME = re.compile(r'(?P<amt>\d+)\s*(?P<unit>\w+)')


class TownShell(Cmd):
    prompt = 'TS $ '
    intro = INTRO
    town = None
    config_path = None

    def do_run(self, line):
        match = RE_TIME.match(line.strip().lower())
        if not match:
            print('Must match format "8 hours" or "10 minutes", etc')
            return
        amt = int(match.group('amt'))
        unit = match.group('unit')
        tdelta = timedelta(**{unit: amt})
        self.town.run(tdelta)
        print('Current time: {}'.format(self.town.now.isoformat()))

    def do_printactions(self, line):
        for npc in self.town.npcs:
            npc.print_action_history()

    def do_load(self, path):
        if not os.path.exists(path):
            if path:
                print('{} does not exist.'.format(path))
            else:
                print('Please enter a path to a configuration or town.')
        else:
            self.config_path = path
            self.town = Town(config_path=self.config_path)
            print(self.town)

    def do_reload(self, line):
        if self.config_path:
            self.town = Town(config_path=self.config_path)
            print(self.town)
        else:
            print('Please load a configuration first.')

    def do_print(self, line):
        if self.town is None:
            print('Please load a town or config with "load <path>" command.')
        else:
            print(self.town)

    def do_exit(self, line):
        print('')
        return True

    def do_EOF(self, line):
        print('')
        return True


def main():
    TownShell().cmdloop()


if __name__ == '__main__':
    main()
