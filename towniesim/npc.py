'''
towniesim.npc
=============

Generate a random NPC

from .job import (ApothecaryJob, ButcherJob, HerbalistJob, ClothierJob,
                  ShoemakerJob, SpinsterJob, BakerJob, BarberJob,
                  BlacksmithJob, ArmorerJob, BowyerJob, AtilliatorJob,
                  FletcherJob, CookJob, ServerJob, ArtistJob, CandlemakerJob,
                  CarpenterJob, MoneylenderJob, ClerkJob, PotterJob,
                  LibrarianJob, ScribeJob, FishermanJob, FarmerJob,
                  BartenderJob, ForesterJob, Job)
'''
from random import choice
from datetime import timedelta
from .chrono import Shift
from .behave import ActionPlan, Plan
from .namerator import make_name
from .race import Race
from .job import WorkHistory
from .util import nds

CLASSES = ['fighter', 'wizard', 'rogue', 'cleric', 'ranger', 'barbarian',
           'druid', 'monk', 'bard', 'paladin', 'sorcerer', 'warlock']


class Memory:

    def __init__(self):
        self.reaction = 0


class NPC:

    def __init__(self, race=None, name=None, level=1, klass=None, job=None,
                 age=None, gender=None, lover=None, family=None,
                 parents_family=None, pos=None):
        race = race or choice(['human', 'half-orc', 'elf', 'dwarf', 'half-elf',
                               'gnome', 'halfling'])
        self.gender = gender or choice(['male', 'female'])
        if isinstance(race, str):
            self.race = Race.get_race(race)()
        elif race is None:
            self.race = race
        else:
            self.race = race()
        self.name = name or make_name(race=race, gender=self.gender)
        self.cash = 0
        self.mood = 0
        self.sleepy = 0
        self.drunk = 0
        self.alcoholism = 0
        self.desired_sleep = 7
        self.level = level
        self.klass = klass
        self.job = job
        self.age = self.race.random_age(age=age)
        self.lover = lover
        self.dead = False
        self.family = family
        self.parents_family = parents_family
        self.work_history = WorkHistory()
        self.pos = pos
        self.racism = None
        self.plans = []
        self.plan_history = []
        self._current_plan = None
        self.npc_history = {}
        self.init_racism()

    def __repr__(self):
        age = self.race.get_age(self.age).value
        if self.job:
            return '{race}_{age}_{gender}_{job!r}'.format(
                age=age,
                race=self.race.NAME.title(),
                gender=self.gender,
                job=self.job,
            )
        elif self.klass:
            return '{race}_{age}_{gender}_{klass!r}'.format(
                age=age,
                gender=self.gender.title(),
                race=self.race.NAME.title(),
                level=self.level,
                klass=self.klass,
            )
        else:
            return '{race}_{age}_{gender}'.format(
                age=age,
                race=self.race.NAME.title(),
                gender=self.gender,
            )

    def init_racism(self):
        self.racism = {}
        for r in Race.get_races():
            self.racism[r] = nds(5, 10) - nds(5, 10)
            self.racism[r] += r.RACISM
        self.racism[self.race.__class__] -= 25

    def take_last_name(self, other):
        last = other.get_last_name()
        if last is None:
            raise ValueError('No name for significant other: {!r}'.format(other))
            return None
        myspl = self.name.split()
        self.name = '{} {}'.format(myspl[0], last).title()
        return last

    def long_r(self):
        return (
            'NPC(name="{s.name}", race="{s.race!r}", age={s.age}, '
            'level={s.level}, klass={s.klass!r}, job={s.job!r})'
            .format(s=self)
        )

    def get_last_name(self):
        spl = self.name.split()
        if len(spl) == 1:
            raise ValueError('No name for self: {!r}'.format(self))
            return None
        return spl[-1].title()

    def get_age(self):
        return self.race.get_age(self.age)

    def set_job(self, job):
        self.job = job
        self.work_history.new_job(job)
        job.location.add_worker(self)

    def sum_history(self, other):
        history = self.npc_history.get(other, [])
        s = 0
        for memory in history:
            s += memory.reaction
        return s

    def friendly(self, other):
        racism = self.racism[other.race.__class__]
        history_mod = self.sum_history(other)
        return racism + self.mood + history_mod

    def make_plans(self, dt):
        if self.job:
            start, end = self.job.shift.dtrange(dt)
            self.plans.append(Plan(ActionPlan.work, start, end))
            self.plans.append(Plan.rand_sleep(dt, self.job.shift,
                                              hours=self.desired_sleep))
        else:
            self.plans.append(Plan.rand_sleep(dt, Shift.mid,
                                              hours=self.desired_sleep))
        self.sort_plans()

    def current_plan(self, dt):
        if self._current_plan is None or self._current_plan.end < dt:
            old_plan = self._current_plan
            if self.plans and dt in self.plans[-1]:
                self._current_plan = self.plans.pop()
            else:
                self._current_plan = self.new_plan(dt)
            if old_plan is None:
                self.plan_history.append((dt, ActionPlan.null,
                                          self._current_plan.action))
            elif old_plan.action != self._current_plan.action:
                self.plan_history.append((dt, old_plan.action,
                                          self._current_plan.action))
        return self._current_plan

    def sort_plans(self):
        self.plans.sort(key=lambda x: x.start, reverse=True)

    def new_plan(self, dt):
        plan_time = timedelta(hours=1)
        if self.plans:
            plan_time = min(self.plans[-1].start - dt, timedelta(hours=1))
        return Plan(ActionPlan.rest, dt, dt + plan_time)

    def print_action_history(self):
        print('{s.name} Action History'.format(s=self))
        for dt, old, new in self.plan_history:
            print('{dtiso}: {old.value} => {new.value}'.format(
                dtiso=dt.isoformat(), old=old, new=new))
