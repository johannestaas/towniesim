'''
towniesim.area
==============
'''
from random import choice


class Position:

    def __init__(self, arg):
        if isinstance(arg, Position):
            return arg
        self.ref = arg

    @property
    def x(self):
        if isinstance(self.ref, tuple):
            return self.ref[0]
        return self.ref.x

    @property
    def y(self):
        if isinstance(self.ref, tuple):
            return self.ref[1]
        return self.ref.y

    @property
    def typ(self):
        return self.ref.__class__


class Block:

    def __init__(self, x, y, in_town=False):
        self.pos = Position((x, y))
        self.block = []
        self.development = None
        self.in_town = in_town

    def char(self):
        if self.development:
            return self.development.char()
        if self.block:
            return str(len(self.block))
        if self.in_town:
            return ' '
        return '.'

    def add(self, building):
        self.block.append(building)

    def full(self):
        return len(self.block) >= 9


class Area:

    def __init__(self, config):
        self.area = None
        self.config = config
        self.width = int(self.config['width'])
        self.height = int(self.config['height'])
        self.townx = int((self.width - int(self.config['town_width'])) / 2)
        self.towny = int((self.height - int(self.config['town_height'])) / 2)
        self.make_area()
        self.land_blocks = [x for x in self.iter_land_blocks()]
        self.town_blocks = [x for x in self.iter_town_blocks()]

    def make_area(self):
        self.area = []
        for x in range(self.width):
            col = []
            for y in range(self.height):
                if self.in_town(x, y):
                    col += [Block(x, y, in_town=True)]
                else:
                    col += [Block(x, y)]
            self.area += [col]

    def in_town(self, x, y):
        if x < self.townx or x >= self.townx + int(self.config['town_width']):
            return False
        if y < self.towny or y >= self.towny + int(self.config['town_height']):
            return False
        return True

    def __getitem__(self, p):
        if isinstance(p, Position):
            return self.area[p.x][p.y]
        else:
            return self.area[p[0]][p[1]]

    def repr_row(self, y):
        s = ''
        for x in range(self.width):
            b = self.area[x][y]
            s += b.char()
        return s

    def __repr__(self):
        s = '+' + '-' * self.width + '+\n'
        for y in range(self.height):
            s += '|' + self.repr_row(y) + '|\n'
        s += '+' + '-' * self.width + '+\n'
        return s

    def iter_land_blocks(self):
        for col in self.area:
            for block in col:
                if not block.in_town:
                    yield block

    def iter_town_blocks(self):
        for col in self.area:
            for block in col:
                if block.in_town:
                    yield block

    def random_land_block(self):
        return choice(self.land_blocks)

    def random_town_block(self):
        return choice(self.town_blocks)
