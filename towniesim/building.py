'''
towniesim.shop
==============
'''
from .area import Position, Block
from .job import (ApothecaryJob, ButcherJob, HerbalistJob, ClothierJob,
                  ShoemakerJob, SpinsterJob, BakerJob, BarberJob,
                  BlacksmithJob, ArmorerJob, BowyerJob, AtilliatorJob,
                  FletcherJob, CookJob, ServerJob, ArtistJob, CandlemakerJob,
                  CarpenterJob, MoneylenderJob, ClerkJob, PotterJob,
                  LibrarianJob, ScribeJob, FishermanJob, FarmerJob,
                  BartenderJob)


class MetaBuilding(type):
    SHOPS = {}

    def __new__(cls, name, bases, dct):
        new_cls = super(MetaBuilding, cls).__new__(cls, name, bases, dct)
        if name in ('Building', 'Residence'):
            return new_cls
        MetaBuilding.SHOPS[dct['NAME']] = new_cls
        if 'JOBS' not in dct:
            dct['JOBS'] = []
        if 'LEVEL' not in dct:
            dct['LEVEL'] = 0
        return new_cls


class Building(metaclass=MetaBuilding):

    def __init__(self, owner=None, affluence=0, workers=None,
                 inventory=None, occupants=None, pos=None, **kwargs):
        self.pos = Position(pos)
        if isinstance(pos, Block):
            pos.add(self)
        self.owner = owner
        self.affluence = affluence
        self.workers = workers or set()
        self.inventory = inventory or set()
        self.occupants = occupants or set()

    @classmethod
    def get_buildings(cls):
        return MetaBuilding.SHOPS.values()

    @classmethod
    def get_building(cls, name):
        return MetaBuilding.SHOPS[name]

    def add_worker(self, npc):
        self.workers.add(npc)

    def __repr__(self):
        return '{s.NAME}[{s.owner.name}]'.format(s=self)


class BakeryBuilding(Building):
    NAME = 'bakery'
    LEVEL = 1
    JOBS = [BakerJob]


class BlacksmithBuilding(Building):
    NAME = 'blacksmith'
    LEVEL = 1
    JOBS = [ArmorerJob, BlacksmithJob, BowyerJob, AtilliatorJob, FletcherJob]


class ButcherBuilding(Building):
    NAME = 'butchery'
    LEVEL = 1
    JOBS = [ButcherJob]


class ClothierBuilding(Building):
    NAME = 'clothier'
    LEVEL = 1
    JOBS = [ClothierJob, ShoemakerJob, SpinsterJob]


class PotteryBuilding(Building):
    NAME = 'potter'
    LEVEL = 1
    JOBS = [PotterJob]


class FishBuilding(Building):
    NAME = 'fishmonger'
    LEVEL = 1
    JOBS = [FishermanJob]


class FarmerBuilding(Building):
    NAME = 'produce'
    LEVEL = 1
    JOBS = [FarmerJob]


class CarpentryBuilding(Building):
    NAME = 'carpenter'
    LEVEL = 1
    JOBS = [CarpenterJob]


class ApothecaryBuilding(Building):
    NAME = 'apothecary'
    LEVEL = 2
    JOBS = [ApothecaryJob, HerbalistJob]


class CandleBuilding(Building):
    NAME = 'candlemaker'
    LEVEL = 2
    JOBS = [CandlemakerJob]


class LibraryBuilding(Building):
    NAME = 'library'
    LEVEL = 2
    JOBS = [LibrarianJob, ScribeJob]


class TavernBuilding(Building):
    NAME = 'tavern'
    LEVEL = 2
    JOBS = [BartenderJob, CookJob, ServerJob]


class RestaurantBuilding(Building):
    NAME = 'restaurant'
    LEVEL = 3
    JOBS = [CookJob, ServerJob]


class ArtBuilding(Building):
    NAME = 'gallery'
    LEVEL = 3
    JOBS = [ArtistJob]


class BankBuilding(Building):
    NAME = 'bank'
    LEVEL = 3
    JOBS = [MoneylenderJob, ClerkJob]


class BarberBuilding(Building):
    NAME = 'barbershop'
    LEVEL = 3
    JOBS = [BarberJob]
