'''
towniesim.development
=====================
'''
from .job import FarmerJob, ForesterJob, MinerJob, FishermanJob
from .resource import Dirt, Iron, Copper, Silver, Gold, Fish, Lumber


class MetaDevelopment(type):
    DEVELOPMENTS = {}
    RESOURCE2DEV = {}

    def __new__(cls, name, bases, dct):
        new_cls = super(MetaDevelopment, cls).__new__(cls, name, bases, dct)
        if name == 'Development':
            return new_cls
        MetaDevelopment.DEVELOPMENTS[dct['NAME']] = new_cls
        for res in dct['RESOURCES']:
            MetaDevelopment.RESOURCE2DEV[res] = new_cls
        return new_cls


class Development(metaclass=MetaDevelopment):

    def __init__(self, town=None):
        self.town = town
        self.workers = set()
        self.block = None

    @classmethod
    def get_developments(cls):
        return MetaDevelopment.DEVELOPMENTS.values()

    @classmethod
    def get_development(cls, name):
        return MetaDevelopment.DEVELOPMENTS[name]

    @classmethod
    def get_from_resource(cls, res):
        return MetaDevelopment.RESOURCE2DEV[res.__class__]

    def add_worker(self, npc):
        self.workers.add(npc)

    def set_block(self, block):
        self.block = block
        block.development = self

    def __repr__(self):
        return '{name}({workers!r})'.format(
            name=self.NAME.title(),
            workers=self.workers,
        )


class Farm(Development):
    NAME = 'farm'
    WORKER = FarmerJob
    RESOURCES = [Dirt]
    # OUTPUT = [Produce]

    def char(self):
        return 'F'


class Mine(Development):
    NAME = 'mine'
    WORKER = MinerJob
    RESOURCES = [Iron, Copper, Silver, Gold]

    def char(self):
        return 'M'


class Harbor(Development):
    NAME = 'harbor'
    WORKER = FishermanJob
    RESOURCES = [Fish]

    def char(self):
        return 'H'


class Sawmill(Development):
    NAME = 'sawmill'
    WORKER = ForesterJob
    RESOURCES = [Lumber]

    def char(self):
        return 'S'
