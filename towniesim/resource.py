'''
towniesim.resource
==================
'''
from .util import nds


class MetaResource(type):
    RESOURCES = {}

    def __new__(cls, name, bases, dct):
        new_cls = super(MetaResource, cls).__new__(cls, name, bases, dct)
        if name == 'Resource':
            return new_cls
        MetaResource.RESOURCES[dct['NAME']] = new_cls
        return new_cls

    def __repr__(cls):
        return cls.NAME


class Resource(metaclass=MetaResource):

    def __init__(self, amt):
        self.amount = amt

    @classmethod
    def get_resources(cls):
        return MetaResource.RESOURCES.values()

    @classmethod
    def get_resource(cls, name):
        return MetaResource.RESOURCES[name]

    @classmethod
    def random(cls):
        amt = nds(5, 10)
        return cls(amt)

    def __repr__(self):
        return '{s.NAME}[{s.amount}]'.format(s=self)

    def __lt__(self, other):
        return self.amount * self.VALUE < other.amount * other.VALUE


class Dirt(Resource):
    NAME = 'dirt'
    VALUE = 1


class Fish(Resource):
    NAME = 'fish'
    VALUE = 2


class Lumber(Resource):
    NAME = 'lumber'
    VALUE = 5


class Iron(Resource):
    NAME = 'iron'
    VALUE = 5


class Copper(Resource):
    NAME = 'copper'
    VALUE = 10


class Silver(Resource):
    NAME = 'silver'
    VALUE = 100


class Gold(Resource):
    NAME = 'gold'
    VALUE = 1000


Ores = (Iron, Copper, Silver, Gold)
