'''
towniesim.config
================
'''
from configobj import ConfigObj
from .util import random_from_list
from .race import Race


class TownConfig:

    def __init__(self, config_path=None, config=None):
        self.conf = {}
        if config_path:
            self.conf.update(ConfigObj(config_path))
        if config:
            self.conf.update(config)

    def random_race(self):
        return Race.get_race(random_from_list(self.conf['races']))

    def random_klass(self):
        return random_from_list(self.conf['classes'])

    def num_families(self):
        return int(self.conf.get('num_settler_families', 20))

    def num_farms(self):
        return int(self.conf.get('num_settler_farms', 5))

    def __getitem__(self, index):
        return self.conf[index]
