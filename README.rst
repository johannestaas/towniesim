towniesim
=========

simulate a pre-designed town with specified and random parameters

Installation
------------

From the project root directory::

    $ python setup.py install

Usage
-----

Simply run it::

    $ towniesim

Use --help/-h to view info on the arguments::

    $ towniesim --help

Release Notes
-------------

:0.0.1:
    Project created
