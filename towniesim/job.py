'''
towniesim.job
=============
'''

from enum import Enum
from random import choice
from .chrono import Shift


class MetaJob(type):
    JOBS = {}

    def __new__(cls, name, bases, dct):
        new_cls = super(MetaJob, cls).__new__(cls, name, bases, dct)
        if name == 'Job':
            return new_cls
        MetaJob.JOBS[dct['NAME']] = new_cls
        for key in ('CAN_OWN', 'SUPERVISOR', 'MILITARY', 'WATER'):
            dct[key] = dct.get(key, False)
        for key in ('LEVEL',):
            dct[key] = dct.get(key, 1)
        return new_cls


class Job(metaclass=MetaJob):

    def __init__(self, level=None, location=None, pay=None, shift=None):
        self.pay = pay or self.WAGE
        self.location = location
        self.level = level or self.LEVEL
        self.shift = shift or choice(self.SHIFTS)

    @classmethod
    def get_jobs(cls):
        return MetaJob.JOBS.values()

    @classmethod
    def get_job(cls, name):
        return MetaJob.JOBS[name]

    def __repr__(self):
        return '{name}L{level}[{pay}]'.format(
            name=self.NAME.title(), level=self.level, pay=self.pay
        )


class ApothecaryJob(Job):
    NAME = 'apothecary'
    WAGE = 40
    CAN_OWN = True
    LEVEL = 1
    SHIFTS = (Shift.day, Shift.mid)


class ButcherJob(Job):
    NAME = 'butcher'
    WAGE = 10
    CAN_OWN = True
    LEVEL = 1
    SHIFTS = (Shift.day, Shift.mid)


class HerbalistJob(Job):
    NAME = 'herbalist'
    WAGE = 40
    CAN_OWN = True
    LEVEL = 1
    SHIFTS = (Shift.day, Shift.mid, Shift.eve)


class ClothierJob(Job):
    NAME = 'clothier'
    WAGE = 30
    CAN_OWN = True
    LEVEL = 1
    SHIFTS = (Shift.day, Shift.mid)


class ShoemakerJob(Job):
    NAME = 'shoemaker'
    WAGE = 30
    CAN_OWN = True
    LEVEL = 1
    SHIFTS = (Shift.day, Shift.mid)


class SpinsterJob(Job):
    NAME = 'spinster'
    WAGE = 5
    LEVEL = 1
    SHIFTS = (Shift.day, Shift.mid, Shift.eve)


class BakerJob(Job):
    NAME = 'baker'
    WAGE = 20
    CAN_OWN = True
    LEVEL = 1
    SHIFTS = (Shift.day, Shift.mid,)


class BarberJob(Job):
    NAME = 'barber'
    WAGE = 10
    CAN_OWN = True
    LEVEL = 2
    SHIFTS = (Shift.day, Shift.mid,)


class BlacksmithJob(Job):
    NAME = 'blacksmith'
    WAGE = 100
    CAN_OWN = True
    LEVEL = 1
    SHIFTS = (Shift.day, Shift.mid,)


class ArmorerJob(Job):
    NAME = 'armorer'
    WAGE = 100
    CAN_OWN = True
    LEVEL = 1
    SHIFTS = (Shift.day, Shift.mid,)


class BowyerJob(Job):
    NAME = 'bowyer'
    WAGE = 50
    CAN_OWN = True
    LEVEL = 1
    SHIFTS = (Shift.day, Shift.mid,)


class AtilliatorJob(Job):
    NAME = 'atilliator'
    WAGE = 80
    CAN_OWN = True
    LEVEL = 1
    SHIFTS = (Shift.day, Shift.mid,)


class FletcherJob(Job):
    NAME = 'fletcher'
    WAGE = 20
    LEVEL = 1
    SHIFTS = (Shift.day, Shift.mid,)


class SailorJob(Job):
    NAME = 'sailor'
    WAGE = 5
    WATER = True
    LEVEL = 1
    SHIFTS = (Shift.grave, Shift.day, Shift.mid, Shift.eve)


class MessengerJob(Job):
    NAME = 'messenger'
    WAGE = 2
    LEVEL = 2
    SHIFTS = (Shift.day, Shift.mid, Shift.eve)


class MinerJob(Job):
    NAME = 'miner'
    WAGE = 10
    LEVEL = 1
    SHIFTS = (Shift.day,)


class LaborerJob(Job):
    NAME = 'laborer'
    WAGE = 5
    LEVEL = 1
    SHIFTS = (Shift.day, Shift.mid, Shift.eve)


class CookJob(Job):
    NAME = 'cook'
    WAGE = 40
    CAN_OWN = True
    LEVEL = 1
    SHIFTS = (Shift.day, Shift.mid, Shift.eve)


class ServerJob(Job):
    NAME = 'server'
    WAGE = 3
    LEVEL = 1
    SHIFTS = (Shift.day, Shift.mid, Shift.eve)


class ArtistJob(Job):
    NAME = 'artist'
    WAGE = 50
    CAN_OWN = True
    LEVEL = 3
    SHIFTS = (Shift.day, Shift.mid, Shift.eve)


class CandlemakerJob(Job):
    NAME = 'candlemaker'
    WAGE = 10
    CAN_OWN = True
    LEVEL = 1
    SHIFTS = (Shift.day, Shift.mid,)


class CarpenterJob(Job):
    NAME = 'carpenter'
    WAGE = 60
    CAN_OWN = True
    LEVEL = 1
    SHIFTS = (Shift.day, Shift.mid,)


class MoneylenderJob(Job):
    NAME = 'moneylender'
    WAGE = 100
    CAN_OWN = True
    LEVEL = 3
    SHIFTS = (Shift.day, Shift.mid,)


class ClerkJob(Job):
    NAME = 'clerk'
    WAGE = 50
    LEVEL = 3
    SHIFTS = (Shift.day, Shift.mid,)


class PotterJob(Job):
    NAME = 'potter'
    WAGE = 20
    CAN_OWN = True
    LEVEL = 1
    SHIFTS = (Shift.day, Shift.mid,)


class LibrarianJob(Job):
    NAME = 'librarian'
    WAGE = 50
    CAN_OWN = True
    LEVEL = 2
    SHIFTS = (Shift.day, Shift.mid, Shift.eve)


class ScribeJob(Job):
    NAME = 'scribe'
    WAGE = 50
    CAN_OWN = True
    LEVEL = 2
    SHIFTS = (Shift.day, Shift.mid,)


class GuardJob(Job):
    NAME = 'guard'
    WAGE = 20
    MILITARY = True
    LEVEL = 2
    SHIFTS = (Shift.grave, Shift.day, Shift.mid, Shift.eve)


class GuardCaptainJob(Job):
    NAME = 'guardcaptain'
    WAGE = 100
    SUPERVISOR = True
    MILITARY = True
    LEVEL = 3
    SHIFTS = (Shift.day, Shift.mid, Shift.eve)


class ShipCaptainJob(Job):
    NAME = 'shipcaptain'
    WAGE = 100
    SUPERVISOR = True
    WATER = True
    LEVEL = 2
    SHIFTS = (Shift.grave, Shift.day, Shift.mid, Shift.eve)


class FishermanJob(Job):
    NAME = 'fisherman'
    WAGE = 10
    WATER = True
    LEVEL = 1
    SHIFTS = (Shift.grave, Shift.day, Shift.mid, Shift.eve)


class FarmerJob(Job):
    NAME = 'farmer'
    WAGE = 10
    CAN_OWN = True
    LEVEL = 1
    SHIFTS = (Shift.day,)


class BartenderJob(Job):
    NAME = 'bartender'
    WAGE = 20
    CAN_OWN = True
    LEVEL = 1
    SHIFTS = (Shift.mid, Shift.eve, Shift.grave)


class ForesterJob(Job):
    NAME = 'forester'
    WAGE = 10
    CAN_OWN = True
    LEVEL = 1
    SHIFTS = (Shift.day,)


class WorkHistory:

    def __init__(self):
        self.job_history = []
        self.loc_xp = {}
        self.job_xp = {}
        self.loc = None
        self.job_type = None

    def new_job(self, job):
        self.loc = job.location
        self.job_type = job.__class__
        self.job_history += [job]
        loc_exp = self.loc_xp.get(self.loc, {})
        loc_exp[self.job_type] = loc_exp.get(self.job_type, 0)
        self.loc_xp[self.loc] = loc_exp
        self.job_xp[self.job_type] = self.job_xp.get(self.job_type, 0)
